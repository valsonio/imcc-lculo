﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMC_cálculo
{
    class Usuario
    {
        public string Nome { get; set; }
        public double Peso { get; set; }
        public double Altura { get; set; }
        public string Idade { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }


    }
}
