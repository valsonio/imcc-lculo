﻿using System;

namespace IMC_cálculo
{
    class Program
    {
        static void Main(string[] args)
        {
            Usuario usuario = new Usuario();
            usuario.Nome = "Valsonio";
            usuario.Peso = 75;
            usuario.Altura = 1.74;


            CalculoIMC imc = new CalculoIMC();

            double valorIMC = imc.CalcularIMC(usuario);
            Console.WriteLine(valorIMC);
          
            
            string classificacao = imc.ClassificarIMC(valorIMC);
            Console.WriteLine(classificacao);

        }
        //public string CalculoIMC(Usuario Calculo)
        //{

        //}


    }
}
